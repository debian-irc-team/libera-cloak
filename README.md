# Cloaks for the Libera IRC Network

Group contacts for the Libera network are Joerg Jaspert <joerg> and Stuart Prescott <stuart>. They arrange the cloaks with Libera staff.

To request a cloak, please open an issue in this salsa project and state your nick on Libera. Note that the nick must be fully registered.
Cloaks are of the format *debian/$ACCOUNT*, where $ACCOUNT is the Debian login.

Please use the [Cloak Request](https://salsa.debian.org/debian-irc-team/libera-cloak/-/issues/new?issuable_template=Cloak%20Request) issue template, thanks.

## IRC Bots
If you happen to run an IRC bot for Debian you can [ask for a cloak](https://salsa.debian.org/debian-irc-team/libera-cloak/-/issues/new?issuable_template=Cloak%20Request%20Bot) too, they will get a *debian/ircbot/$NICK* account.
